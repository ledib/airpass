
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

// import { url } from "inspector";

// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.procod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

const urlAPI = 'http://localhost:47848/api/';
export const environment = {
  production: false,
  
  mainUrl: urlAPI,
  User:{
    login: urlAPI + 'User/Login', 
    register:urlAPI +'User/RegisterUser'
  },
  Tickets: {
    genericTickets: urlAPI + 'home/GetTicketsList',
    genericTicket:urlAPI +'home/GetTicket/',
    addNewTicket:urlAPI + 'home/AddTicket',
    sameTicketexistance:urlAPI + 'home/IsExistingTicket/',
    deleteTicket:urlAPI +'home/DeleteTicket/',
    genericTicketFilterCount:urlAPI+'home/GetTicketFilterCount/',
    genericTicketFilter:urlAPI+'home/GetTicketFilter/',
   
  },
  TicketType: {
    genericTicketTypeList: urlAPI + 'tickettype/GetTicketType',
    getPriceById:urlAPI+"ticketType/PriceByTicketId/"
},
  
  
 
   
  };


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
