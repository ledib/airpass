import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(private httpClient:HttpClient) { }
  public login(username:string, password:string){
    const body={
      Username:username,
      Password:password
    }
   return this.httpClient.post(environment.User.login,body);
  }
  public register(username:string,fullname:string,email:string, password:string){
    const body={
      Username:username,
      FullName:fullname,
      Email:email,
      Password:password
    }
   return this.httpClient.post(environment.User.register,body);
  }
}
