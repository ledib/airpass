import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import {
  HttpClient,
  HttpErrorResponse,
  HttpParams,
  HttpHeaders
} from '@angular/common/http';
export interface ITicketsData {
  id: number;
  inbound: string;
  outbound: string;
  ticket_type: number;
ticket_type_description:string;
ticket_type_id:number;
  price: number;
  from_date: Date;
  to_date: Date;
  seat_number:number;
}
export class TicketsData implements ITicketsData{
  id: number;
  inbound: string;
  outbound: string;
  ticket_type: number;
  ticket_type_description:string;
  ticket_type_id:number;
  price: number;
  from_date: Date;
  to_date: Date;
  seat_number:number;

}
export interface TableData extends Array<TicketsData> { }
@Injectable({
  providedIn: 'root'
})
export class HomeService {

  constructor(private http:HttpClient) { 

  }
  getData() {
    return this.http.get<TableData>(environment.Tickets.genericTickets)
  }
  getTicketFilter(page: number, row: number, filter_inbound: string, filter_outbound: string
    // ,filter_from_date:string,filter_to_date:string
    ) {
      debugger;
    if(filter_inbound=="")
    filter_inbound ="null";
  
  if(filter_outbound=="")
    filter_outbound ="null";
    var url = environment.Tickets.genericTicketFilter + page + "/" + row + "/" + filter_inbound + "/" + filter_outbound 
    // +"/"+filter_from_date +"/"+filter_to_date;
    return this.http.get<TableData>(url)
      .pipe(
        catchError(this.handleError)
      );
  }
  getTicketFilterCount(filter_inbound: string, filter_outbound: string
    // ,filter_from_date:string,filter_to_date:string
    ) {
  debugger;
  if(filter_inbound=="")
    filter_inbound ="null";
  
  if(filter_outbound=="")
    filter_outbound ="null";
  //  filter_from_date == "" ? "null" : filter_from_date;
  //  filter_to_date  == "" ? "null" : filter_to_date;
    var url = environment.Tickets.genericTicketFilterCount+filter_inbound + "/" + filter_outbound 
    // +"/"+filter_from_date+"/"+filter_to_date;
    return this.http
      .get<number>(url)
      .pipe(
       // retry(environment.retryHttpRequestNumber), // retry a failed request up to 3 times
        catchError(this.handleError)
      );
  }
  getTicket(id: number) {
    debugger
    return this.http.get<TicketsData>(environment.Tickets.genericTicket + id)
   }
   delete(id:number){
    return this.http.delete(environment.Tickets.deleteTicket + id)
   }
  upsert(model: TicketsData) {
   const body={
    id:model.id,
    inbound:model.inbound,
    outbound:model.outbound,
   ticket_type:model.ticket_type,
    price:model.price,
    from_date:model.from_date,
    to_date:model.to_date,
    seat_number:model.seat_number
  }
  console.log(body);
    return this.http
      .post<any>(
        environment.Tickets.addNewTicket,
        body
        // ,{headers:headerPost}
        ).pipe(
          catchError(this.handleError)
        );
  }
  isExisting(inbound: string,outbound:string,from_date:Date,to_date:Date,seat_number:number) {
    debugger;
    return this.http.get<any>(environment.Tickets.sameTicketexistance + inbound + "/" + outbound + "/" + from_date +"/" + to_date + "/" + seat_number )
      .pipe(

        catchError(this.handleError),

      );
  }
  getPriceByTicketType(ticketid:number){
    debugger;
    return this.http.get<any>(environment.TicketType.getPriceById + ticketid)
  }
  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    return throwError('Something bad happened; please try again later.');

  }
}
