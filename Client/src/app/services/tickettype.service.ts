import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { catchError, retry } from 'rxjs/operators';
import {
  HttpClient,
  HttpErrorResponse,
  HttpParams,
  HttpHeaders
} from '@angular/common/http';
export interface ITicketTypeData {
  id: number;
  ticket_type_description:string;
}

export class TicketTypeData implements ITicketTypeData {
  id: number;
  ticket_type_description:string;
}
export interface TableData extends Array<ITicketTypeData> { }
@Injectable({
  providedIn: 'root'
})

export class TickettypeService {

  constructor(private http:HttpClient) { }
  getTicketType() {
    return this.http.get<TableData>(environment.TicketType.genericTicketTypeList )
  }
}
