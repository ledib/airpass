import { CommonModule } from '@angular/common';
import { NgModule,NO_ERRORS_SCHEMA  } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, NgModel, ReactiveFormsModule } from '@angular/forms';
import { NG2DataTableModule } from 'angular2-datatable-pagination';
import { BrowserModule } from '@angular/platform-browser';
import { HomeComponent } from './home.component';
describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        NgModule,
        BrowserModule,
        NG2DataTableModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule],
      declarations: [ HomeComponent ],
      schemas:[NO_ERRORS_SCHEMA]

    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
