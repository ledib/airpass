import { Component, OnInit,NgModule } from '@angular/core';
import { Router } from '@angular/router';
import { constants } from '../Helper/constants';
import { ToastrService } from 'ngx-toastr';
import { environment } from 'src/environments/environment';
import { HomeService } from '../services/home.service';
import { TableData } from '../services/home.service';
import * as moment from 'moment';
import { User } from '../Models/user';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  //styleUrls: ['./home.component.css','../../../../scss/vendors/toastr/toastr.scss'],
  providers: [ HomeService,ToastrService]
})
export class HomeComponent implements OnInit {
  error: any;
  public checkrole:boolean=false;
  public data: TableData;
  public filter_inbound= '';
  public filter_outbound= '';
  public filter_from_date= '';
  public filter_to_date= '';
  public page = 1;
  public row = 10;
  public itemsTotal:any;
  totalRecords: number;
  constructor(private router: Router, private service:HomeService,private toastr: ToastrService) { 
  
  this.itemsTotal=0;
  
  }

  ngOnInit(): void {
    const user = JSON.parse(localStorage.getItem(constants.USER_KEY) || '{}') as User;
    if(user.roles=='Admin')
       this.checkrole=true;
       else
       this.checkrole=false;
       //this.cerca();
      this.service.getData().subscribe((data: any) => {
      this.data = [...data];
      this.totalRecords=this.data.length;
    }, // success path
    error => this.error = error // error path
  );

  }
 
  cercaClick() {
    this.row = 10;
    this.page = 1;
    this.cerca();
  }
  cerca() {
    debugger;
    this.service.getTicketFilterCount(this.filter_inbound, this.filter_outbound
      // , this.filter_from_date,this.filter_to_date
      ).subscribe((data: any) => {
        debugger;
      console.log(this.itemsTotal);
   
      this.itemsTotal = data;
      console.log(this.itemsTotal)
      this.service.getTicketFilter(this.page, this.row, this.filter_inbound, this.filter_outbound
        // ,this.filter_from_date,this.filter_to_date
        )
        .subscribe(
          (data: any) => {
            this.data = [...data];
          }, 
        );
    }, 
      error => this.error = error 
    );

  }
  public onPageChange(event:any) {
    this.row = event.rowsOnPage;
    this.page = event.activePage;
    this.cerca();
  }
  formatDate(date: Date) {
    if (date != null)
      return moment.utc(date).local().format('DD-MM-YYYY');
    else
      return "";
  }
  onLogout() {
    localStorage.removeItem(constants.USER_KEY);
    this.router.navigate(["/"]);

  }
  get isUserLogin(){
    const user = JSON.parse(localStorage.getItem(constants.USER_KEY) || '{}') as User;
    //const user=localStorage.getItem("userInfo");
    return user && user!=null;
  
 }
 reset() {
   this.filter_inbound="";
   this.filter_outbound=""
  this.ngOnInit();
}
 delete(id:number){
   debugger;
  this.service.delete(id).subscribe(
    (coop: any) => {
      this.service.getData().subscribe((data: any) => {
        this.data = [...data];
        this.toastr.success('Record deleted successfully!','Success!');
       }, 
      error => this.error = error 
    );
});
 }

}
