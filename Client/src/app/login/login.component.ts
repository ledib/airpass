import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';
import { constants } from '../Helper/constants';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

 

export class LoginComponent implements OnInit {
  title = 'Client';
  message = '';
  public loginForm=this.formBuilder.group({
    username:['',Validators.required],
    password:['',Validators.required]
  })
  public registerForm=this.formBuilder.group({
    fullname:['',Validators.required],
    username:['',Validators.required],
    email:['',Validators.required],
    password:['',Validators.required]
  }
  )
  constructor(private formBuilder:FormBuilder, private userService:UserService,public router: Router) { }

  ngOnInit(): void {
    
  }
  get f() {
    return this.loginForm.controls;
    
  }

onSubmit(){
  let username= this.loginForm.controls.username.value;
  let password= this.loginForm.controls.password.value;
  this.userService.login(username,password).subscribe((data:any)=>{
    if (data.responseCode == 1) {
      localStorage.setItem(constants.USER_KEY,JSON.stringify(data.dateSet));
       this.router.navigate(["/home"]);
      }
 
    },
    error => {
      this.message = 'Accesso negato';
    
  })
  
}
onSubmitRegister(){
  let username= this.registerForm.controls.username.value;
  let fullname=this.registerForm.controls.fullname.value;
  let email=this.registerForm.controls.email.value;
  let password= this.registerForm.controls.password.value;
  this.userService.register(username,fullname,email,password).subscribe((data)=>{
     this.router.navigate(["/"]);
  })
}
}
