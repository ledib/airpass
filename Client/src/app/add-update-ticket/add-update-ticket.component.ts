import { Component, OnInit,Input } from '@angular/core';
import { HomeService, TicketsData } from '../services/home.service';
import { TickettypeService, TicketTypeData } from '../services/tickettype.service';
import { FormGroup,FormControl, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-add-update-ticket',
  templateUrl: './add-update-ticket.component.html',
  styleUrls: ['./add-update-ticket.component.css'],
  providers: [HomeService,ToastrService],
})
export class AddUpdateTicketComponent implements OnInit {
  @Input()
  ticket: TicketsData;
  ticket_id: number;
  isEditMode: boolean = false;
 currentPrice:number;
  test:any;
  listaTicket: Array<TicketsData>;
  TicketForm: FormGroup;
  listaTicketType: Array<TicketTypeData>;
  constructor(private _fb: FormBuilder,
    private service: HomeService,
    private route: ActivatedRoute,
    private router: Router,
    private servicett:TickettypeService,
    private toastr: ToastrService
    ) 
    {
    this.ticket = new TicketsData();
    this.listaTicketType = new Array<TicketTypeData>();
    this.servicett.getTicketType().subscribe(data => (this.listaTicketType = data));
     }

  ngOnInit() {
    this.TicketForm = this._fb.group({
      id: ['', [Validators.required]],
     inbound: ['', [Validators.required]],
      outbound: ['',[ Validators.required]],
      ticket_type: ['', [Validators.required]],
      price: ['',[Validators.required]],
      from_date: ['',[Validators.required]],
      to_date:['',[Validators.required]],
      seat_number:['',[Validators.required]]
    });
    this.route.paramMap.subscribe(() => {
     const param=this.route.snapshot.paramMap.get('id');
     this.ticket_id =param?+param:0;
      if (this.ticket_id == -1) {
       this.ticket.id = -1;
        this.ticket.inbound = "";
        this.ticket.outbound = "";
        this.ticket.ticket_type = 0;
        //this.ticket.ticket_type_id = 0;
        this.ticket.price = 0;
        this.ticket.from_date=new Date(),
        this.ticket.to_date=new Date(),
        this.ticket.seat_number= 0;
    
        this.TicketForm.controls.id.setValue(
          this.ticket_id
          
        );
      } else {
        this.isEditMode = true;
        this.service.getTicket(this.ticket_id).subscribe(tick => {
         this.ticket = tick;
          this.TicketForm.controls.id.setValue(
            tick.id
          );
          this.TicketForm.controls.inbound.setValue(
            tick.inbound
          );
          this.TicketForm.controls.outbound.setValue(
            tick.outbound
           );
           this.TicketForm.controls.ticket_type.setValue(
            tick.ticket_type
           );
           this.TicketForm.controls.price.setValue (
            tick.price
           );
           this.TicketForm.controls.from_date.setValue (
            this.formatDate(tick.from_date) 
           );
           this.TicketForm.controls.to_date.setValue (
            this.formatDate(tick.to_date)  
           );
           this.TicketForm.controls.seat_number.setValue (
            tick.seat_number
           );
           this.currentPrice=tick.price;
        });
      }
    });



  }
 
  reset() {
    this.router.navigate(['/home']);
  }
  get f() {
    return this.TicketForm.controls;
    
  }

  onSubmit() {
    const invalid = new Array<string>();
    const controls = this.TicketForm.controls;
    for (const name in controls) {
      if (controls[name].invalid) {
        invalid.push(name);
      }
    }
    
    if (this.TicketForm.invalid )
      return;
     else{
        this.service.isExisting(this.TicketForm.controls.inbound.value,this.TicketForm.controls.outbound.value,this.TicketForm.controls.from_date.value,this.TicketForm.controls.to_date.value,this.TicketForm.controls.seat_number.value).subscribe(result => {
 
        if (result) {
          this.toastr.warning('This record already exists!','Warning!');
         return;
         
        }
        else
       {

        this.ticket.inbound = this.TicketForm.controls.inbound.value;
        this.ticket.outbound = this.TicketForm.controls.outbound.value;
        this.ticket.ticket_type = this.TicketForm.controls.ticket_type.value;
        this.ticket.price = this.TicketForm.controls.price.value;
        this.ticket.from_date = this.TicketForm.controls.from_date.value;
        this.ticket.to_date = this.TicketForm.controls.to_date.value;
        this.ticket.seat_number = this.TicketForm.controls.seat_number.value;
        
        this.service.upsert(this.ticket).subscribe(
          (data: any) => {
            this.isEditMode = true;
            this.ticket = data;
            this.toastr.success('Success', 'Data added/updated successfully');
             this.router.navigateByUrl('/home');
            
         });
        }
      });
      
    }
  
  }
  formatDate(date:Date) {
    let d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();
   if (month.length < 2) {
      month = '0' + month;
    }
    if (day.length < 2) {
      day = '0' + day;
    }
 return [year, month, day].join('-');
  }
  onChangeTicketType(newValue:number){
    if(newValue==6 || newValue==7){
    this.service.getPriceByTicketType(newValue).subscribe(data => {
      this.TicketForm.controls.price.setValue(data);
    })
    
  }
  else if(this.isEditMode){
    this.TicketForm.controls.price.setValue(this.currentPrice);
  } 
  else this.TicketForm.controls.price.setValue("");
 }
}
