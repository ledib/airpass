import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AuthGuardService } from './guards/auth.service';
import { AddUpdateTicketComponent } from './add-update-ticket/add-update-ticket.component';
const routes: Routes = [
  { path: 'test', component: AppComponent },
  { path: '', component: LoginComponent },
  { path: 'home', component: HomeComponent ,canActivate:[AuthGuardService]},
  { path: 'ticketform/:id', component: AddUpdateTicketComponent ,canActivate:[AuthGuardService]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents=[AppComponent,LoginComponent,HomeComponent,AddUpdateTicketComponent]