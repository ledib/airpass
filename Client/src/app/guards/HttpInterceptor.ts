import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, from } from 'rxjs';
import { Router } from '@angular/router';
import { tap } from 'rxjs/operators';
import { AuthGuardService } from '../guards/auth.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    constructor(private router: Router,private service:AuthGuardService) {}

     intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return from(this.handle(req, next))
      }
    
      async handle(req: HttpRequest<any>, next: HttpHandler) {
    
        const idToken = localStorage.getItem('access_token'); 
        if (idToken) {
          const headers = new HttpHeaders().set('Authorization', 'Bearer ' + idToken);
          req = req.clone({ headers: headers });
        }
    
        return next.handle(req).pipe(tap(() => { },
          (err: any) => {
            if (err instanceof HttpErrorResponse) {
              if (err.status !== 401) {
                return;
              }
              //this.service.logout();
              this.router.navigate(['/login']);      
            }
          })).toPromise();
      }
}
