﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Server.Data;
using Server.Managers.Interface;
using Server.Models;
using Server.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Managers
{
    public class HomeManager : IHomeManager
    {
        private readonly AppDBContext _context;
        private readonly ILogger<HomeManager> _logger;
        public HomeManager(AppDBContext context, ILogger<HomeManager> logger)
        {
            this._context = context;
            this._logger = logger;
        }
        public List<TicketsViewModel> GetList()
        {
            List<TicketsViewModel> ticketsList = new List<TicketsViewModel>();
            try
            {
                using (var db = this._context)
                {
                    var query =
                       from ticket in db.Tickets
                       join type in db.TicketType on ticket.Ticket_type equals type.Id
                       select new TicketsViewModel
                       {
                           Id = ticket.Id,
                           Ticket_type = ticket.Ticket_type,
                           Ticket_type_description = type.Ticket_type_description,
                           Inbound = ticket.Inbound,
                           Outbound = ticket.Outbound,
                           Price = ticket.Price,
                           From_date = ticket.From_date,
                           To_date = ticket.To_date,
                           Seat_number = ticket.Seat_number

                       };

                    ticketsList = query.ToList();
                    return ticketsList;

                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return ticketsList;
            }

        }

        public Task<Tickets> GetById(int Id)
        {
            var ticket = _context.Tickets.Where(ss => ss.Id == Id).First();
            return Task.FromResult(ticket);
        }
        public async Task<int> SaveTicket(TicketsViewModel model)
        {
            try
            {
                using (var dbContextTransaction = _context.Database.BeginTransaction())
                {
                    if (model.Id == -1)
                    {
                        var newticket = new Tickets();
                        // newticket.Id = model.Id;
                        newticket.Inbound = model.Inbound.TrimEnd();
                        newticket.Outbound = model.Outbound.TrimEnd();
                        newticket.Ticket_type = model.Ticket_type;
                        newticket.Price = model.Price;
                        newticket.From_date = model.From_date;
                        newticket.To_date = model.To_date;
                        newticket.Seat_number = model.Seat_number;
                        _context.Add(newticket);
                    }
                    else
                    {
                        var _ticket = _context.Tickets.Where(ss => ss.Id == model.Id).First();
                        _ticket.Inbound = model.Inbound.TrimEnd();
                        _ticket.Outbound = model.Outbound.TrimEnd();
                        _ticket.Ticket_type = model.Ticket_type;
                        _ticket.From_date = model.From_date;
                        _ticket.To_date = model.To_date;
                        _ticket.Price = model.Price;
                        _ticket.Seat_number = model.Seat_number;

                    }
                    _context.SaveChanges();
                    dbContextTransaction.Commit();
                    return await Task.FromResult(1);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return await Task.FromResult(0);
            }

        }
        public bool TicketExists(string inbound, string outbound, DateTime from_date, DateTime to_date, int seat_number)
        {
            var ticketExists = _context.Tickets.Any(x => x.Inbound == inbound && x.Inbound == outbound && x.From_date == from_date && x.To_date == to_date && x.Seat_number == seat_number);
            if (ticketExists)
                return true;
            return false;

        }
        public Task<int> Delete(int Id)
        {
            try
            {
                using (var dbContextTransaction = _context.Database.BeginTransaction())
                {
                    var ticket = _context.Tickets.Where(ss => ss.Id == Id).First();
                    _context.Tickets.Remove(ticket);
                    _context.SaveChanges();
                    dbContextTransaction.Commit();
                    return Task.FromResult(1);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return Task.FromResult(0);
            }
        }
        public int GetTicketCount(string inbound = "", string outbound = ""/*,string filter_from_date="",string filter_to_date=""*/)
        {
            int dbResult = 0;
            try
            {
                string query = @"SELECT * FROM Tickets o";

                if ( inbound!="null" && outbound == "null")
                    query += @" where o.inbound LIKE '%" + inbound + "%'";

                else if ( outbound != "null" && inbound == "null")
                    query += @"  where o.outbound LIKE '%" + outbound + "%' ";
                else if ( inbound != "null" && outbound != "null")
                    query += @"  where o.inbound LIKE '%" + inbound + "%' and o.outbound LIKE '"+outbound +"%' ";
                //bool from = false;
                //if (filter_to_date != "" && filter_from_date != "null")
                //{
                //    from = true;
                //    query += @"  and o.from_date >= @from_date ";
                //}
                //bool to = false;
                //if (filter_to_date != "" && filter_to_date != "null")
                //{
                //    to = true;
                //    query += @"  and o.to_date <= @to_date ";
                //}


                dbResult = _context.Tickets.FromSqlRaw(query).Count();

                return dbResult;

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return dbResult;
            }
        }
        public List<TicketsViewModel> GetTicketFilter(int page, int row, string inbound = "", string outbound = ""/*,string from_date="",string todate=""*/)
        {
            List<TicketsViewModel> ticketsListViewModel = new List<TicketsViewModel>();
            List<Tickets> ticketsList = new List<Tickets>();
            try
            {
                {
                    string query = @"SELECT o.* FROM Tickets o";
                    if ( inbound != "null"  && outbound == "null")
                        query += @" where o.inbound LIKE '%" + inbound + "%'";

                    else if (/*!string.IsNullOrEmpty(outbound)*/ outbound != "null"  && inbound == "null")
                        query += @"  where o.outbound LIKE '%" + outbound + "%' ";
                    else if (  inbound != "null"  && outbound != "null")
                        query += @"  where o.inbound LIKE '%" + inbound + "%' and o.outbound LIKE '" + outbound + "%' ";

                    // and so on for all combinations

                   // query += @" order by  o.id ";

                    //if (page >= 0)
                    //{
                    //    page = (page - 1) * row;
                    //    query += @"OFFSET " + page + " ROWS FETCH NEXT " + row + " ROWS ONLY";
                    //}

                    ticketsList = _context.Tickets.FromSqlRaw(query).ToList();
                    for (int i =0; i <= ticketsList.Count(); i++)
                    {
                        
                        var query1 =
                        from ticket in _context.Tickets
                        join type in _context.TicketType on ticket.Ticket_type equals type.Id
                        where ticket.Id == ticketsList[i].Id
                        select new TicketsViewModel
                        {
                            Id = ticket.Id,
                            Ticket_type = ticket.Ticket_type,
                            Ticket_type_description = type.Ticket_type_description,
                            Inbound = ticket.Inbound,
                            Outbound = ticket.Outbound,
                            Price = ticket.Price,
                            From_date = ticket.From_date,
                            To_date = ticket.To_date,
                            Seat_number = ticket.Seat_number
                        };
                        
                        ticketsListViewModel.Add(query1.FirstOrDefault());
                    }


                    return ticketsListViewModel;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return ticketsListViewModel;
            }
        }

    }
}