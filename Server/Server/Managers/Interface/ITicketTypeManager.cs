﻿using Server.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Managers.Interface
{
    public interface ITicketTypeManager
    {
        List<TicketType> GetTicketTypeList();
        decimal GetPriceById(int ticketId);

    }
}
