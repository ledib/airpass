﻿
using Server.Models;
using Server.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Managers.Interface
{
    public interface IHomeManager
    {
        List<TicketsViewModel> GetList();
        Task<Tickets> GetById(int Id);
        Task<int> SaveTicket(TicketsViewModel model);
        bool TicketExists(string inbound, string outbound, DateTime from_date, DateTime to_date, int seat_number);
        Task<int> Delete(int Id);
        List<TicketsViewModel> GetTicketFilter(int pagina, int righe, string inbound = "", string outbound = ""/*,string filter_from_date="",string a=""*/);
        int GetTicketCount(string inbound = "", string outbound = ""/*,string datefrom="",string dateto=""*/);
    }
}
