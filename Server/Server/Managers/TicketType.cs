﻿using Microsoft.Extensions.Logging;
using Server.Data;
using Server.Managers.Interface;
using Server.Models;
using Server.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Managers
{
    public class TicketTypeManager : ITicketTypeManager
    {
        private readonly AppDBContext _context;
        private readonly ILogger<TicketTypeManager> _logger;
        public TicketTypeManager(AppDBContext context, ILogger<TicketTypeManager> logger)
        {
            this._context = context;
        }
        public List<TicketType> GetTicketTypeList()
        {
            List<TicketType> ticketsList = new List<TicketType>();
            try
            {
                using (var db = this._context)
                {
                    var query =
                       from ticket in db.TicketType
                       select new TicketType
                       {
                           Id = ticket.Id,
                           Ticket_type_description=ticket.Ticket_type_description
                          };

                    ticketsList = query.ToList();
                    return ticketsList;

                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return ticketsList;
            }

        }
        public decimal GetPriceById(int ticketId)
        {
            decimal newprice=0;
            if (ticketId == 6)
            {
                var fee = from f in _context.Tickets
                          where f.Ticket_type == ticketId
                          select f.Price;

                 newprice = fee.Max();
            }
            if (ticketId == 7)
            {
                var fee = from f in _context.Tickets
                          where f.Ticket_type == ticketId
                          select f.Price;

                newprice = fee.Min();

            }
            return newprice;
        }
    }
}
