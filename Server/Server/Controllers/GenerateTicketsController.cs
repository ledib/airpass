﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Server.Managers.Interface;
using Server.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Controllers
{
    [ApiController]
    [Authorize(Roles = "Admin")]
    [Route("api/[controller]")]
    public class GenerateTicketsController : Controller
    {
        private readonly IHomeManager _home;
        public  GenerateTicketsController(IHomeManager home )
        {
             _home = home;
        }
    [HttpGet]
        [Route("[action]/{number}")]
        public void GenerateTicketRecords(int number)
    {
        var ticketViewModel = new TicketsViewModel();
        for (int i = 1; i <= number; i++)
        {
            ticketViewModel.Id = -1;
            ticketViewModel.Inbound = "inbound" + i;
            ticketViewModel.Outbound = "outbound" + i;
            ticketViewModel.Ticket_type = this.GetRandomNumber();
            ticketViewModel.Price = 50 + i;
            ticketViewModel.Seat_number = 1 + i;
            ticketViewModel.From_date = this.RandomDay();
            ticketViewModel.To_date = this.RandomDay();
            _home.SaveTicket(ticketViewModel);
        }



    }
    [HttpGet]
    [Route("[action]")]
    public int GetRandomNumber()
    {
        Random rnd = new Random();
        int[] tickettype = { 1, 2, 3, 4, 5, 6, 7 };

        int mIndex = rnd.Next(1, tickettype.Length);



        return tickettype[mIndex];
    }
    private Random gen = new Random();
    [HttpGet]
    [Route("[action]")]
    public DateTime RandomDay()
    {
        DateTime start = new DateTime(1995, 1, 1);
        int range = (DateTime.Today - start).Days;
        return start.AddDays(gen.Next(range));
    }
}
}
