﻿

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Server.Managers.Interface;
using Server.Models;
using Server.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IHomeManager _home;
        public HomeController(IHomeManager home, ILogger<HomeController> logger)
        {
            _logger = logger;
            _home = home;
        }
        [HttpGet]
        [Route("[action]/{inbound}/{outbound}")]
        public int GetTicketFilterCount(string inbound, string outbound/*,string filter_from_date,string filter_to_date*/)
        {
             
            int result = _home.GetTicketCount(inbound, outbound/*,filter_from_date,filter_to_date*/);
            return result;

        }

        [HttpGet]
        [Route("[action]/{page}/{row}/{filter_inbound}/{filter_outbound}")]
        public ActionResult GetTicketFilter(int page, int row, string filter_inbound, string filter_outbound/*,string filter_from_date,string filter_to_date*/)
        {


            IEnumerable<TicketsViewModel> result = _home.GetTicketFilter(page, row, filter_inbound, filter_outbound/*, filter_from_date,filter_to_date*/);

            return Ok(result);

        }
        [HttpGet]
        [Route("[action]")]
        public List<TicketsViewModel> GetTicketsList()
        {
            return _home.GetList();
        }
        [HttpGet]
        [Route("[action]/{id}")]
        public Tickets GetTicket(int id)
        {
            return _home.GetById(id).Result;
        }


        [HttpPost("AddTicket")]
        public ActionResult<int> AddTicket(TicketsViewModel ticketViewModel)
        {
            var resultCreate = _home.SaveTicket(ticketViewModel).Result;
            if (resultCreate == 0)
                return StatusCode(500, "Internal server error");
            else
                return Ok(resultCreate);
        }
        [HttpGet]
        [Route("[action]/{inbound}/{outbound}/{from_date}/{to_date}/{seat_number}")]
        public bool IsExistingTicket(string inbound, string outbound, DateTime from_date, DateTime to_date, int seat_number)
        {
            return _home.TicketExists(inbound, outbound, from_date, to_date, seat_number);
        }
        [HttpDelete]
        [Route("[action]/{id}")]
        public ActionResult<int> DeleteTicket(int id)
        {
            var resultDelete = _home.Delete(id).Result;
            if (resultDelete == 0)
                return StatusCode(500, "Internal server error");
            else
                return Ok(resultDelete);
        }
       
    }

}
