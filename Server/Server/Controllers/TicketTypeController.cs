﻿
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Server.Managers.Interface;
using Server.Models;
using Server.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    
    public class TicketTypeController : Controller
    {
        private readonly ITicketTypeManager _ticketType;
        public TicketTypeController(ITicketTypeManager ticketType)
        {
            _ticketType = ticketType;
        }

        [HttpGet]
        [Route("[action]")]
        public List<TicketType> GetTicketType()
        {
            return _ticketType.GetTicketTypeList();
        }

       [HttpGet]
        [Route("[action]/{id}")]
        public decimal PriceByTicketId(int id)
        {
            return _ticketType.GetPriceById(id);
        }
        
    }

}
