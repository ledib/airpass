﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Models
{
    public class TicketType
    {
        [Key]
        [Required]
        public int Id { get; set; }
        [Required]
        public string Ticket_type_description { get; set; }
    }
}
