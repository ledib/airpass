﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Models.ViewModels
{
    public class TicketsViewModel
    {
        [Key]
        [Required]
        public int Id { get; set; }
        [Required]
        public string Inbound { get; set; }
        [Required]
        public string Outbound { get; set; }
        [Required]
        public int Ticket_type { get; set; }
     
        public string Ticket_type_description { get; set; }

        
        public int Ticket_type_id { get; set; }
        [Required]
        public decimal Price { get; set; }
        [Required]
        public DateTime From_date { get; set; }
        public DateTime? To_date { get; set; }
        [Required]
        public int Seat_number { get; set; }
    }
}
