﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Models
{
    public class Tickets

    {
        [Key]
        [Required(ErrorMessage = "This field is required")]
        public int Id { get; set; }
        [Required(ErrorMessage = "This field is required")]
        public string Inbound { get; set; }
        [Required(ErrorMessage = "This field is required")]
        public string Outbound { get; set; }
        [Required(ErrorMessage = "This field is required")]
        public int Ticket_type { get; set; }

        public int? Ticket_type_id { get; set; }
        [Required(ErrorMessage = "This field is required")]
        public decimal Price { get; set; }
        [Required(ErrorMessage = "This field is required")]
        public DateTime From_date { get; set; }
        public DateTime? To_date { get; set; }
        [Required(ErrorMessage = "This field is required")]
        public int Seat_number { get; set; }


    }
}
