﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Models.DTO
{
    public class UserDTO
    {
        public UserDTO(string fullName, string email, string userName, List<string> roles)
        {
            FullName = fullName;
            Email = email;
            UserName = userName;
            Roles = roles;

        }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public List<string> Roles { get; set; }
        public string Token { get; set; }


    }
}
